
import java.sql.*;
import javax.swing.DefaultListModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ameen
 */
public class ManageInventory extends javax.swing.JFrame {
    final String url = "jdbc:postgresql://localhost:5432/grocery_store1";
    final String user = "postgres";
    final String password = "Basketball1";
    private StaffMember sm;
    /**
     * Creates new form ManageInventory
     */
    public ManageInventory(StaffMember sm) {
        this.sm = sm;
        initComponents();
        createInventoryList();
    }
    
    public void createInventoryList()
    {
        String getStock = "select * from stock";
        String getItemName = "select name from product where id = ?";
        try{
                 Connection conn = DriverManager.getConnection(url, user, password);
                 Statement stockStatement = conn.createStatement();
                 ResultSet rs = stockStatement.executeQuery(getStock);
                 DefaultListModel dlm = new DefaultListModel();
                  
                 while(rs.next())
                  {
                      PreparedStatement ps = conn.prepareStatement(getItemName);
                      ps.setString(1, rs.getString("product_id"));
                      ResultSet productName = ps.executeQuery();
                      productName.next();
                      String name = productName.getString("name");
                      String stock = rs.getString("product_id") + "-" + rs.getString("warehouse_id") + "-" + name + "-" + rs.getInt("amount");
                      dlm.addElement(stock);
                  }
                 
                  stockList.setModel(dlm);
    	 }catch (SQLException e) {
                  System.out.println(e.getMessage());
         }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        stockList = new javax.swing.JList<>();
        amountSpinner = new javax.swing.JSpinner();
        addInventory = new javax.swing.JButton();
        back = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        stockList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(stockList);

        amountSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        addInventory.setText("Add");
        addInventory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addInventoryActionPerformed(evt);
            }
        });

        back.setText("Back");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 283, Short.MAX_VALUE)
                        .addComponent(amountSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addInventory))
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(back)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addInventory)
                    .addComponent(amountSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(back)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addInventoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addInventoryActionPerformed
        // TODO add your handling code here:
        String getInventorySql = "select amount from stock where product_id = ? and warehouse_id = ?";
        String addInventorySql = "update stock set amount = ? where product_id = ? and warehouse_id = ?";
        try{
                 Connection conn = DriverManager.getConnection(url, user, password);
                 PreparedStatement ps = conn.prepareStatement(addInventorySql);
                 PreparedStatement ps2 = conn.prepareStatement(getInventorySql);
                 int amount = (int) amountSpinner.getValue();
                 String productID = stockList.getSelectedValue().substring(0, 5);
                 String warehouseID = stockList.getSelectedValue().substring(6, 11);
                 System.out.println(productID + " " + warehouseID);
               
                 ps2.setString(1, productID);
                 ps2.setString(2, warehouseID);
                 ResultSet rs = ps2.executeQuery();
                 rs.next();
                 amount += rs.getInt("amount");
                 
                 ps.setInt(1, amount);
                 ps.setString(2, productID);
                 ps.setString(3, warehouseID);
                 
                 ps.executeUpdate();
                 createInventoryList();
                 amountSpinner.setValue(1);
                 
    	 }catch (SQLException e) {
                  System.out.println(e.getMessage());
         }
        
    }//GEN-LAST:event_addInventoryActionPerformed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        // TODO add your handling code here:
        new StaffMemberForm(sm).setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_backActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManageInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManageInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManageInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManageInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new ManageInventory().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addInventory;
    private javax.swing.JSpinner amountSpinner;
    private javax.swing.JButton back;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> stockList;
    // End of variables declaration//GEN-END:variables
}
