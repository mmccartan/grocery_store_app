/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ameen
 */

import java.sql.*;
import java.util.*;
import java.util.logging.*;
import java.io.*;
public class DataScript {

    /**
     * @param args the command line arguments
     */


    
    public void runScript() {
        final String url = "jdbc:postgresql://localhost:5432/grocery_store1";
        final String user = "postgres";
        final String password = "Basketball1";//
        String sql = "insert into stock values (?, ?, ?)";
    	String fileName = "stock.txt";
        File file = new File(fileName);    
    	try{
                 Class.forName("org.postgresql.Driver");
                 Connection conn = DriverManager.getConnection(url, user, password);
                 //Statement stmt = conn.createStatement();

                 //conn.setAutoCommit(false);
                 System.out.println("Connected to the PostgreSQL server successfully."); 
                 //stmt.executeUpdate(sql1);
                 //conn.commit();
                 
                 try{
                     Scanner scan = new Scanner(file);
                    
                     while(scan.hasNext()){
                         PreparedStatement ps = conn.prepareStatement(sql);
                         String data = scan.next();
                         String[] values = data.split("-");
                         ps.setString(1, values[0]);
                         ps.setString(2, values[1]);
                         ps.setInt(3, Integer.parseInt(values[2]));
                         ps.executeUpdate();
                       
                     }
                     scan.close();
                 }
                 catch(FileNotFoundException e){
                         e.printStackTrace();
                 }
                 
    	 }catch (SQLException e) {
                  System.out.println(e.getMessage());
    	} catch (ClassNotFoundException ex) {
            Logger.getLogger(DataScript.class.getName()).log(Level.SEVERE, null, ex);
        }
         
      
    }
    
}