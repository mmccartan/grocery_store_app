
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ameen
 */
public class StaffMember {
     final String url = "jdbc:postgresql://localhost:5432/grocery_store1";
     final String user = "postgres";
     final String password = "Basketball1";
     
     private String id;
     
    public StaffMember(String id)
    {
        this.id = id;
        try{
                 Class.forName("org.postgresql.Driver");
                 Connection conn = DriverManager.getConnection(url, user, password);
    		 System.out.println("Connected to the PostgreSQL server successfully."); 
     
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	} catch (ClassNotFoundException ex) {
             Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public String getName()
    {
        String sql = "select first_name, last_name " +  "from staff_member " + "where ID = ?";
        String name = "";
        try(Connection conn = DriverManager.getConnection(url, user, password); 
                 PreparedStatement ps = conn.prepareStatement(sql)){ 
                 ps.setString(1, this.id);
                 ResultSet rs = ps.executeQuery();
                 rs.next();
                 name = rs.getString("first_name") + " " + rs.getString("last_name");
                 
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	}
        return name;
    }
    
    public boolean staffMemberExists()
     {
        String sql = "select * " +  "from staff_member " + "where ID = ?";
        String staff = "";
        try(Connection conn = DriverManager.getConnection(url, user, password); 
                 PreparedStatement ps = conn.prepareStatement(sql)){ 
                 ps.setString(1, this.id);
                 ResultSet rs = ps.executeQuery();
                 rs.next();
                 staff = this.id + " " + rs.getString("first_name") + " " + rs.getString("middle_name") + " " + rs.getString("last_name");
                 
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	}
        return !staff.isEmpty();
     }
}
