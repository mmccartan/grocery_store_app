
import java.sql.*;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ameen
 */
public class AddDeleteProductForm extends javax.swing.JFrame {
    private StaffMember sm;
    final String url = "jdbc:postgresql://localhost:5432/grocery_store1";
    final String user = "postgres";
    final String password = "Basketball1";
    String lastID = " ";
    /**
     * Creates new form AddDeleteProductForm
     */
    public AddDeleteProductForm(StaffMember sm) {
        this.sm = sm;
        initComponents();
        createProductList();
        clearTextFields();
    }
    
    public void clearTextFields()
    {
        nameField.setText("");
        categoryField.setText("");
        additionalInfoField.setText("");
        sizeField.setText("");
    }
    
    public void createProductList()
    {
        String sql = "select * from product";
        try{
                 Connection conn = DriverManager.getConnection(url, user, password);
    		 System.out.println("Connected to the PostgreSQL server successfully."); 
                 Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(sql);
                 DefaultListModel dlm = new DefaultListModel();
                 String product = "";
                 
                 while(rs.next())
                  {
                      product = rs.getString("id") + "-" + rs.getString("name") + "-" + rs.getString("category") + "-" + rs.getString("additional_info") + "-" + rs.getInt("size");
                      dlm.addElement(product);
                  }
                  
                  lastID = product.substring(0,5);
                  productList.setModel(dlm);

               
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	} 
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        productList = new javax.swing.JList<>();
        nameField = new javax.swing.JTextField();
        categoryField = new javax.swing.JTextField();
        additionalInfoField = new javax.swing.JTextField();
        sizeField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        add = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        modify = new javax.swing.JButton();
        goBack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        productList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(productList);

        nameField.setText("jTextField1");

        categoryField.setText("jTextField2");
        categoryField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categoryFieldActionPerformed(evt);
            }
        });

        additionalInfoField.setText("jTextField3");

        sizeField.setText("jTextField4");

        jLabel1.setText("Name");

        jLabel2.setText("Category");

        jLabel3.setText("Additional Info");

        jLabel4.setText("Size");

        add.setText("Add");
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        delete.setText("Delete");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });

        modify.setText("Modify");
        modify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifyActionPerformed(evt);
            }
        });

        goBack.setText("Back");
        goBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                goBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(nameField, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(categoryField, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3)
                    .addComponent(additionalInfoField, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(sizeField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(add)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(delete)
                        .addGap(102, 102, 102)
                        .addComponent(modify)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(goBack)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(goBack)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(categoryField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(additionalInfoField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sizeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(add)
                    .addComponent(delete)
                    .addComponent(modify))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        // TODO add your handling code here:
        String sql = "insert into product values(?, ?, ?, ?, ?)";
        try{
                 int last = Integer.parseInt(lastID.substring(4,5));
                 String id = lastID.substring(0,4) + (last + 1);
                 Connection conn = DriverManager.getConnection(url, user, password);
                 PreparedStatement ps = conn.prepareStatement(sql);
                 ps.setString(1, id);
                 ps.setString(2, this.nameField.getText());
                 ps.setString(3, this.categoryField.getText());
                 ps.setString(4, this.additionalInfoField.getText());
                 ps.setInt(5, Integer.parseInt(this.sizeField.getText()));
                 ps.executeUpdate();
                 createProductList();
                 clearTextFields();
    	 }catch (SQLException e) {
                  System.out.println(e.getMessage());
         }
    }//GEN-LAST:event_addActionPerformed

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        // TODO add your handling code here:
        String sql = "delete from product where id = ?";
        
        try{
                 Connection conn = DriverManager.getConnection(url, user, password);
                 conn.setAutoCommit(false);
                 PreparedStatement ps = conn.prepareStatement(sql);
                 ps.setString(1, productList.getSelectedValue().substring(0,5));
                 ps.executeUpdate();
                 conn.commit();
                 createProductList();
                 clearTextFields();
    	 }catch (SQLException e) {
                  System.out.println(e.getMessage());
         }
    }//GEN-LAST:event_deleteActionPerformed

    private void modifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifyActionPerformed
        // TODO add your handling code here:
        String sql = "update product set name = ?, category = ?, additional_info = ?, size = ? where id = ?";
        String getProduct = "select * from product where id = ?";
         try{
                 Connection conn = DriverManager.getConnection(url, user, password);
                 PreparedStatement ps = conn.prepareStatement(sql);
                 PreparedStatement ps1 = conn.prepareStatement(getProduct);
                 
                 ps1.setString(1, productList.getSelectedValue().substring(0,5));
                 ResultSet rs = ps1.executeQuery();
                 rs.next();
                       
                if(!this.nameField.getText().isEmpty())
                {
                    ps.setString(1, this.nameField.getText());
                }
                else
                {
                    ps.setString(1, rs.getString("name"));
                }
                if(!this.categoryField.getText().isEmpty())
                {
                    ps.setString(2, this.categoryField.getText());
                }
                else
                {
                    ps.setString(2, rs.getString("category"));
                }
                if(!this.additionalInfoField.getText().isEmpty())
                {
                    ps.setString(3, this.additionalInfoField.getText());
                }
                else
                {
                    ps.setString(3, rs.getString("additional_info"));
                }
                if(!this.sizeField.getText().isEmpty())
                {
                    ps.setInt(4, Integer.parseInt(this.sizeField.getText()));
                }
                else
                {
                    ps.setInt(4, rs.getInt("size"));
                }
                ps.setString(5, productList.getSelectedValue().substring(0,5));
                ps.executeUpdate();
                createProductList();
                clearTextFields();
    	 }catch (SQLException e) {
                  System.out.println(e.getMessage());
         }
        
    }//GEN-LAST:event_modifyActionPerformed

    private void goBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_goBackActionPerformed
        // TODO add your handling code here:
        new ModifyProductsForm(sm).setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_goBackActionPerformed

    private void categoryFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categoryFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_categoryFieldActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddDeleteProductForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddDeleteProductForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddDeleteProductForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddDeleteProductForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new AddDeleteProductForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JTextField additionalInfoField;
    private javax.swing.JTextField categoryField;
    private javax.swing.JButton delete;
    private javax.swing.JButton goBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modify;
    private javax.swing.JTextField nameField;
    private javax.swing.JList<String> productList;
    private javax.swing.JTextField sizeField;
    // End of variables declaration//GEN-END:variables
}
