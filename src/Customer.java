
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ameen
 */
public class Customer {
     final String url = "jdbc:postgresql://localhost:5432/grocery_store1";
     final String user = "postgres";
     final String password = "Basketball1";
     
     private String id;
     
    public Customer(String id)
    {
        this.id = id;
    }
    
    public String getID()
    {
        return this.id;
    }
    
    public int getBalance()
    {
        String sql = "select balance " +  "from customer " + "where ID = ?";
        int balance = 0;
        try(Connection conn = DriverManager.getConnection(url, user, password); 
                 PreparedStatement ps = conn.prepareStatement(sql)){ 
                 ps.setString(1, this.id);
                 ResultSet rs = ps.executeQuery();
                 rs.next();
                 balance = rs.getInt("balance");
                 
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	}
        return balance;
    }
    
    public String getName()
    {
        String sql = "select first_name, last_name " +  "from customer " + "where ID = ?";
        String name = "";
        try(Connection conn = DriverManager.getConnection(url, user, password); 
                 PreparedStatement ps = conn.prepareStatement(sql)){ 
                 ps.setString(1, this.id);
                 ResultSet rs = ps.executeQuery();
                 rs.next();
                 name = rs.getString("first_name") + " " + rs.getString("last_name");
                 
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	}
        return name;
    }
    
     public String getCustomer()
    {
        String sql = "select * " +  "from customer " + "where ID = ?";
        String customer = "";
        try(Connection conn = DriverManager.getConnection(url, user, password); 
                 PreparedStatement ps = conn.prepareStatement(sql)){ 
                 ps.setString(1, this.id);
                 ResultSet rs = ps.executeQuery();
                 rs.next();
                 customer = this.id + " " + rs.getString("first_name") + " " + rs.getString("middle_name") + " " + rs.getString("last_name") + " " + rs.getString("balance");
                 
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	}
        return customer;
    }
     
     public boolean customerExists()
     {
        String sql = "select * " +  "from customer " + "where ID = ?";
        String customer = "";
        try(Connection conn = DriverManager.getConnection(url, user, password); 
                 PreparedStatement ps = conn.prepareStatement(sql)){ 
                 ps.setString(1, this.id);
                 ResultSet rs = ps.executeQuery();
                 rs.next();
                 customer = this.id + " " + rs.getString("first_name") + " " + rs.getString("middle_name") + " " + rs.getString("last_name") + " " + rs.getString("balance");
                 
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	}
        return !customer.isEmpty();
     }
     
     public String getState()
     {
        String sql = "select state " +  "from delivery_address " + "where customer_id = ?";
        String state = null;
        try(Connection conn = DriverManager.getConnection(url, user, password); 
                 PreparedStatement ps = conn.prepareStatement(sql)){ 
                 ps.setString(1, this.id);
                 ResultSet rs = ps.executeQuery();
                 rs.next();
                 state = rs.getString("state");
                 
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	}
        return state;
     }

}
