
import java.sql.*;
import javax.swing.DefaultListModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ameen
 */
public class Product {
    private String id;
    private String state;
    final String url = "jdbc:postgresql://localhost:5432/grocery_store1";
    final String user = "postgres";
    final String password = "Basketball1";
    
    public Product(String id, String state)
    {
        this.id = id;
        this.state = state;
    }
    
    public String getID()
    {
        return this.id;
    }
    
    public String getProduct()
    {
        String sql = "select id, name from product where id = ?";
        String product = null;
        try{
                 Connection conn = DriverManager.getConnection(url, user, password);
    		 System.out.println("Connected to the PostgreSQL server successfully."); 
                 PreparedStatement stmt = conn.prepareStatement(sql);
                 stmt.setString(1, this.id);
                 ResultSet rs = stmt.executeQuery();
                 rs.next();
                 product = String.format("%-10s %-" + (30 - rs.getString("name").length()) + "s", rs.getString("id"), rs.getString("name"));
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	} 
        return product;
    }
    
    public double getPrice()
    {
        String sql = "select price from product_pricing where product_id = ? AND state = ?";
        Double price = null;
        try{
                 Connection conn = DriverManager.getConnection(url, user, password);
    		 System.out.println("Connected to the PostgreSQL server successfully."); 
                 PreparedStatement stmt = conn.prepareStatement(sql);
                 stmt.setString(1, this.id);
                 stmt.setString(2, this.state);
                 ResultSet rs = stmt.executeQuery();
                 rs.next();
                 price = rs.getDouble("price");
    	 }catch (SQLException e) {
                 System.out.println(e.getMessage());
    	} 
        return price;
    }
}
